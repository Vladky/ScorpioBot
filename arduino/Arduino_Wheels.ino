
#include <Servo.h> 

int Max21 = 255;
int IN1 = 3;
int IN2 = 5;
int IN3 = 6;
int IN4 = 11;
int red = 12;
int blue = 13;
int green = 10;
char msg[5];
char incomingByte;
String temp;
char inData[6]; // Allocate some space for the string
char inChar=-1; // Where to store the character read
byte index = 0; // Index into array; where to store the character
byte Data[2];
int x, y, bite, led, ledR, ledB, ledG, ledStep, currentSpeed;
Servo rHand, lHand;
boolean SIDEDEBUG=true;

void runForward(int speed){
  analogWrite(IN4, speed);
  analogWrite(IN3, LOW);
  analogWrite(IN1, map(speed,0,255,0,Max21));
  analogWrite(IN2, LOW);
}
void runBack(int speed){
  analogWrite(IN4, LOW);
  analogWrite(IN3, speed);
  analogWrite(IN1, LOW);
  analogWrite(IN2, map(speed,0,255,0,Max21));
}
void runLeft(int speed){
  analogWrite(IN4, speed);
  analogWrite(IN3, LOW);
  analogWrite(IN1, LOW);
  analogWrite(IN2, speed);
}
void runRight(int speed){
  analogWrite(IN4, LOW);
  analogWrite(IN3, speed);
  analogWrite(IN1, speed);
  analogWrite(IN2, LOW);
}
void runFR(int speed){
  analogWrite(IN4, LOW);
  analogWrite(IN3, LOW);
  analogWrite(IN1, speed);
  analogWrite(IN2, LOW);
}
void runBR(int speed){
  analogWrite(IN4, LOW);
  analogWrite(IN3, LOW);
  analogWrite(IN1, LOW);
  analogWrite(IN2, speed);
}
void runFL(int speed){
  analogWrite(IN4, speed);
  analogWrite(IN3, LOW);
  analogWrite(IN1, LOW);
  analogWrite(IN2, LOW);
}
void runBL(int speed){
  analogWrite(IN4, LOW);
  analogWrite(IN3, speed);
  analogWrite(IN1, LOW);
  analogWrite(IN2, LOW);
}
void stop(){
  analogWrite(IN4, LOW);
  analogWrite(IN3, LOW);
  analogWrite(IN1, LOW);
  analogWrite(IN2, LOW);
}


void setup()
{
  pinMode (IN4, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (red, OUTPUT);
  pinMode (blue, OUTPUT);
  pinMode (green, OUTPUT);
  lHand.attach(A0);
  rHand.attach(A1);
  index=0;
  temp="";
  Serial.begin(9600);
  ledR=255;
  ledB=0;
  ledG=0;
  ledStep=2;
}

void loop()
{
  if (Serial.available()) {
    incomingByte = Serial.read();
    if (incomingByte==','){
//      Serial.println(temp);
      x=temp[0]-'0';
      y=temp[1]-'0';
      bite=temp[2]-'0';
      led = temp[3]-'0';
      currentSpeed = temp[4]-'0';
      bite=map(bite,0,9,20,130);
      x=x-5;
      y=y-5;
      /*Serial.print("X: "); Serial.print(x);
      Serial.print(", Y: "); Serial.print(y);
      Serial.print(", Bite: "); Serial.println(bite);*/
      
      //Serial.print("LED: ");
      //Serial.println(led);
    
        
        if (led==1){
        digitalWrite(red,255);
        digitalWrite(green,255);
        digitalWrite(blue,255);
      } else
      {
        digitalWrite(red,0);
        digitalWrite(green,0);
        digitalWrite(blue,0);
      }
      
      lHand.write(bite);
      rHand.write(180-bite);
      if (y>=2*x && y>=-2*x){
        if (SIDEDEBUG)
//        Serial.println("runForward");
        runForward(map(currentSpeed,0,9,0,255));
      }
      if (y>=x/2 && y<2*x){
        if (SIDEDEBUG)
//        Serial.println("runFR");
        runFR(map(currentSpeed,0,9,0,255));
      }
      if (y>=-x/2 && y<x/2){
        if (SIDEDEBUG)
//        Serial.println("runRight");
        runRight(map(currentSpeed,0,9,0,255));
      }
      if (y>=-2*x && y<-x/2){
        if (SIDEDEBUG)
//        Serial.println("runBR");
        runBR(map(currentSpeed,0,9,0,255));
      }
      if (y<2*x && y<-2*x){
        if (SIDEDEBUG)
//        Serial.println("runBack");
        runBack(map(currentSpeed,0,9,0,255));
      }
      if (y>=2*x && y<x/2){
        if (SIDEDEBUG)
//        Serial.println("runBL");
        runBL(map(currentSpeed,0,9,0,255));
      }
      if (y>=x/2 && y<-x/2){
        if (SIDEDEBUG)
//        Serial.println("runLeft");
        runLeft(map(currentSpeed,0,9,0,255));
      }
      if (y>=-x/2 && y<-2*x){
        if (SIDEDEBUG)
//        Serial.println("runFL");
        runFL(map(currentSpeed,0,9,0,255));
      }
      if (y==0 && x==0){ 
        if (SIDEDEBUG)
//        Serial.println("stop");
        stop();
      }
          
      temp="";
    } 
    else{
      temp+=incomingByte;
      delay(2);
    }
  }
    
      /*if (ledStep==0){
          ledG+=16;
          if (ledG>=255){
            ledG=255;
            ledStep=1;
          }
        }
        if (ledStep==1){
          ledR-=16;
          if (ledR<=0){
            ledR=0;
            ledStep=2;
          }
        }
        if (ledStep==2){
          ledB+=16;
          if (ledB>=255){
            ledB=255;
            ledStep=3;
          }
        }
        if (ledStep==3){
          ledG-=16;
          if (ledG<=0){
            ledG=0;
            ledStep=4;
          }
        }
        if (ledStep==4){
          ledR+=16;
          if (ledR>=255){
            ledR=255;
            ledStep=5;
          }
        }
        if (ledStep==5){
          ledB-=16;
          if (ledB<=0){
            ledB=0;
            ledStep=0;
          }
        }*/
  index=0;
  /*if (data == "1") {
    runForward(255);
  }
  else if (data == "2") {
    runBack(255);
  }
  else if (data == "3") {
    runRight(255);
  }
  else if (data == "4") {
    runLeft(255);
  }
  else if (data == "0") {
    stop();
  }*/
}

