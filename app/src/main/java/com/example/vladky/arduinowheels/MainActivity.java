package com.example.vladky.arduinowheels;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class MainActivity extends ActionBarActivity implements SensorEventListener{
    private static final String TAG = "myLogs";
    Handler h;
    final int RECIEVE_MESSAGE = 1;
    private StringBuilder sb = new StringBuilder();
    private ConnectedThread mConnectedThread;
    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = "30:15:01:22:14:15";
    private TextView console;
    private Vibrator vib;
    private MyGraphView joystickView;
    private SeekBar handBar;
    private int handValue, tx, ty, td, ledR, ledG, ledB, led;
    private CheckBox checkRed, checkBlue, checkGreen, checkLED, checkAnalog, checkGyro;
    private Bitmap analogDrawable, digitalDrawable,
            ledoffDrawable, ledonDrawable,
            gyroonDrawable, gyrooffDrawable;
    private Boolean isDigital=true;
    private SensorManager sensorManager;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        console = (TextView) findViewById(R.id.console);
        checkRed = (CheckBox) findViewById(R.id.checkRed);
        checkBlue = (CheckBox) findViewById(R.id.checkBlue);
        checkGreen = (CheckBox) findViewById(R.id.checkGreen);
        checkLED = (CheckBox) findViewById(R.id.checkLED);
        checkAnalog = (CheckBox) findViewById(R.id.checkAnalog);
        checkGyro = (CheckBox) findViewById(R.id.checkGyro);
        handBar = (SeekBar) findViewById(R.id.handBar);

        console.setVisibility(View.GONE);
        checkLED.setEnabled(false);
        checkAnalog.setEnabled(false);
        checkGyro.setEnabled(false);
        handBar.setEnabled(false);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        analogDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.analog256, options);
        analogDrawable = Bitmap.createScaledBitmap(analogDrawable, 256, 256, true);
        digitalDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.digital256, options);
        digitalDrawable = Bitmap.createScaledBitmap(digitalDrawable, 256, 256, true);
        ledoffDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.ledoff256, options);
        ledoffDrawable = Bitmap.createScaledBitmap(ledoffDrawable, 256, 256, true);
        ledonDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.ledon256, options);
        ledonDrawable = Bitmap.createScaledBitmap(ledonDrawable, 256, 256, true);
        gyroonDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.gyroon256, options);
        gyroonDrawable = Bitmap.createScaledBitmap(gyroonDrawable, 256, 256, true);
        gyrooffDrawable = BitmapFactory.decodeResource(getResources(), R.drawable.gyrooff256, options);
        gyrooffDrawable = Bitmap.createScaledBitmap(gyrooffDrawable, 256, 256, true);


        findViewById(R.id.checkAnalog).setBackgroundDrawable(new BitmapDrawable(digitalDrawable));
        findViewById(R.id.checkLED).setBackgroundDrawable(new BitmapDrawable(ledoffDrawable));
        findViewById(R.id.checkGyro).setBackgroundDrawable(new BitmapDrawable(gyrooffDrawable));

        /*Button forwardButton = (Button) findViewById(R.id.ForwardButton);
        Button backButton = (Button) findViewById(R.id.BackButton);
        Button rightButton = (Button) findViewById(R.id.RightButton);
        Button leftButton = (Button) findViewById(R.id.LeftButton);*/
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        final LinearLayout joystickLayout = (LinearLayout) findViewById(R.id.joystickLayout);
        joystickView = new MyGraphView(this);
        joystickLayout.addView(joystickView);
        handBar.setMax(9);
        handBar.setProgress(6);
        handValue=6;
        final Handler handler = new Handler();

        handBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (seekBar.getProgress() % 10 == 0 || !checkGyro.isChecked()) {
                    if (checkGyro.isChecked()) {
                        handValue = seekBar.getProgress() / 10;
                    } else {
                        handValue = seekBar.getProgress();
                    }
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mConnectedThread.write(tx + "" + ty + String.valueOf(handValue) + "" + led + ",");
                        }
                    }, 10);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mConnectedThread.write(tx + "" + ty + String.valueOf(handValue) + "" + led + ",");
                        }
                    }, 10);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        joystickView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        tx = joystickView.getTX();
                        ty = joystickView.getTY();
                        if (isDigital)
                            td=9;
                        else
                            td = joystickView.getTD();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mConnectedThread.write(joystickView.getTX() + "" +
                                        joystickView.getTY() + String.valueOf(handValue) +
                                        "" + led + td + ",");
                            }
                        }, 10);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        tx = joystickView.getTX();
                        ty = joystickView.getTY();
                        if (isDigital)
                            td=9;
                        else
                            td = joystickView.getTD();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mConnectedThread.write(joystickView.getTX() + "" +
                                        joystickView.getTY() + String.valueOf(handValue) +
                                        "" + led + td + ",");
                            }
                        }, 10);
                        break;
                    case MotionEvent.ACTION_UP:
                        tx = 5;
                        ty = 5;
                        td = 0;
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mConnectedThread.write(5 + "" + 5 + String.valueOf(handValue) +
                                        "" + led + td + ",");
                                mConnectedThread.write(5 + "" + 5 + String.valueOf(handValue) +
                                        "" + led + td + ",");
                            }
                        }, 10);
                        break;
                }
                return false;
            }
        });

        checkRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) ledR = 1;
                else ledR = 0;
                mConnectedThread.write(tx + "" + ty + String.valueOf(handValue) + "" + led + td + ",");
            }
        });
        checkBlue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) ledB=1; else ledB=0;
                mConnectedThread.write(tx + "" + ty + String.valueOf(handValue)  + ""+ led + td + ",");
            }
        });
        checkGreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) ledG=1; else ledG=0;
                mConnectedThread.write(tx + "" + ty + String.valueOf(handValue)  + ""+ led + td + ",");
            }
        });
        checkAnalog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isDigital=false;
                    findViewById(R.id.checkAnalog).setBackgroundDrawable(new BitmapDrawable(analogDrawable));
                } else {
                    isDigital=true;
                    findViewById(R.id.checkAnalog).setBackgroundDrawable(new BitmapDrawable(digitalDrawable));
                }
                vib.vibrate(50);
            }
        });
        checkLED.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    led=1;
                    findViewById(R.id.checkLED).setBackgroundDrawable(new BitmapDrawable(ledonDrawable));
                } else {
                    led=0;
                    findViewById(R.id.checkLED).setBackgroundDrawable(new BitmapDrawable(ledoffDrawable));
                }
                mConnectedThread.write(tx + "" + ty + String.valueOf(handValue)  + ""+ led + td + ",");
                vib.vibrate(50);
            }
        });
        checkGyro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    findViewById(R.id.checkGyro).setBackgroundDrawable(new BitmapDrawable(gyroonDrawable));
                    handBar.setMax(90);
                } else{
                    findViewById(R.id.checkGyro).setBackgroundDrawable(new BitmapDrawable(gyrooffDrawable));
                    handBar.setMax(9);
                    handBar.setProgress(6);
                }
                vib.vibrate(50);
            }
        });

                h = new Handler() {
                    public void handleMessage(android.os.Message msg) {
                        switch (msg.what) {
                            case RECIEVE_MESSAGE:                                                   // ���� ������� ��������� � Handler
                                byte[] readBuf = (byte[]) msg.obj;
                                String strIncom = new String(readBuf, 0, msg.arg1);
                                sb.append(strIncom);                                                // ��������� ������
                                int endOfLineIndex = sb.indexOf("\r\n");                            // ���������� ������� ����� ������
                                if (endOfLineIndex > 0) {                                            // ���� ��������� ����� ������,
                                    String sbprint = sb.substring(0, endOfLineIndex);               // �� ��������� ������
                                    sb.delete(0, sb.length());                                      // � ������� sb
//                                    console.setText("Arduino: " + sbprint);             // ��������� TextView
                            /*btn.setEnabled(true);*/
                                }
                                //Log.d(TAG, "...������:"+ sb.toString() +  "����:" + msg.arg1 + "...");
                                break;
                        }
                    }

                    ;
                };

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();

    }

    private void checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if(btAdapter==null) {
            errorExit("Fatal Error", "Bluetooth не поддерживается");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d(TAG, "...Bluetooth включен...");
            } else {
                //Prompt user to turn on Bluetooth
                @SuppressWarnings("static-access")
                Intent enableBtIntent = new Intent(btAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private void errorExit(String title, String message){
        Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "...onResume - попытка соединения...");
        console.setText("...onResume - попытка соединения...");

        // Set up a pointer to the remote node using it's address.
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the
        //     UUID for SPP.
        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        btAdapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        Log.d(TAG, "...Соединяемся...");
        console.setText("...Соединяемся...");
        try {
            btSocket.connect();
            Log.d(TAG, "...Соединение установлено и готово к передачи данных...");
            handBar.setEnabled(true);
            checkAnalog.setEnabled(true);
            checkLED.setEnabled(true);
            checkGyro.setEnabled(true);
            console.setText("Готово");
        } catch (IOException e) {
            try {
                btSocket.close();
                Log.d(TAG,"Error, closing socket...");
            } catch (IOException e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }

        // Create a data stream so we can talk to server.
        Log.d(TAG, "...Создание Socket...");

        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();

        mConnectedThread.write("X:"+joystickView.getTX());
        mConnectedThread.write("Y"+joystickView.getTY());
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "...In onPause()...");

        if (outStream != null) {
            try {
                outStream.flush();
            } catch (IOException e) {
                errorExit("Fatal Error", "In onPause() and failed to flush output stream: " + e.getMessage() + ".");
            }
        }

        try     {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER && checkGyro.isChecked()){
            Log.d("1234",event.values[1]*10+", "+(int)event.values[1]*10);
            handBar.setProgress((int)(100-event.values[1]*10));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
            Log.d(TAG,"On constructor");
        }

        public void run() {
            byte[] buffer = new byte[256];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);        // �������� ���-�� ���� � ���� �������� � �������� ������ "buffer"
                    h.obtainMessage(RECIEVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // ���������� � ������� ��������� Handler
                } catch (IOException e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String message) {
            Log.d(TAG, "...Посылаем данные: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d(TAG, "...������ �������� ������: " + e.getMessage() + "...");
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }
}