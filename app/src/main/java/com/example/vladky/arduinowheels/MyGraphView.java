package com.example.vladky.arduinowheels;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;

public class MyGraphView extends View {
    private Paint mPaint; //объект для параметров рисования графических примитивов
    private Paint mBitmapPaint; //объект для параметров вывода битмапа на холст
    private Bitmap mBitmap, tempBitmap; //сам битмап
    private Canvas mCanvas, tempCanvas; //холст
    private int centerX, centerY;
    private int fontColor, circleColor;
    private int tx,ty;
    private int intD;

    public MyGraphView(Context c) { //конструктор
        super(c);
//создаем объект класса Paint для параметров вывода битмапа на холст
        mBitmapPaint = new Paint(Paint.DITHER_FLAG); // Paint.DITHER_FLAG – для эффекта сглаживания
//создаем объект класса Paint для параметров рисования графических примитивов
        circleColor=Color.argb(50, 0x90, 0x90, 0x90);
        fontColor=Color.rgb(0x30,0x30,0x30);
        mPaint = new Paint();
        mPaint.setAntiAlias(true); //устанавливаем антиалиасинг (сглаживание)
        mPaint.setColor(circleColor);
        mPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        centerY = h/2;
        centerX = w/2;
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        tempBitmap = Bitmap.createBitmap(mBitmap);
        tempCanvas = new Canvas(tempBitmap);
        mCanvas.drawColor(fontColor);
    }
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.drawCircle(centerX, centerY, 300, mPaint);
    }
    float x,y;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        double d;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                d = Math.sqrt(Math.pow(centerX-event.getX(),2)+
                        Math.pow(centerY-event.getY(),2));
                intD = (int) Math.round(d/30);
                if (intD>9) intD=9;
                x=event.getX();
                y=event.getY();
                if (d<200){
                    tempCanvas.drawBitmap(mBitmap, 0, 0, null);
                    mCanvas.drawCircle(event.getX(), event.getY(), 100, mPaint);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                d = Math.sqrt(Math.pow(centerX-event.getX(),2)+
                        Math.pow(centerY-event.getY(),2));
                intD = (int) Math.round(d/35);
                if (intD>9) intD=9;
                Log.d("1234",intD+"");
                if (d<200){
                    mCanvas.drawColor(fontColor);
                    mCanvas.drawBitmap(tempBitmap, 0, 0, null);
                    mCanvas.drawCircle(event.getX(), event.getY(), 100, mPaint);
                    tx=(int)event.getX() - centerX;
                    ty=centerY - (int)event.getY();
                }
                if (d>200&&d<300){
                    double l = 500*(d-200)/200;
                    if (l>=1){
                        l=200/(d-200);
                    }
                    double newX = (centerX+l*event.getX())/(1+l);
                    double newY = (centerY + l * event.getY()) / (1+l);
                    mCanvas.drawColor(fontColor);
                    mCanvas.drawBitmap(tempBitmap, 0, 0, null);
                    mCanvas.drawCircle((int) newX, (int) newY, 100, mPaint);
                    tx = (int) newX - centerX;
                    ty = centerY - (int) newY;
                }
                if (d>300){
                    double l = 2*(d-200)/200;
                    if (l>=1){
                        l=200/(d-200);
                    }
                    double newX = (centerX+l*event.getX())/(1+l);
                    double newY = (centerY+l*event.getY())/(1+l);
                    mCanvas.drawColor(fontColor);
                    mCanvas.drawBitmap(tempBitmap, 0, 0, null);
                    mCanvas.drawCircle((int) newX, (int) newY, 100, mPaint);
                    tx = (int) newX - centerX;
                    ty = centerY - (int) newY;
                }
                break;
            case MotionEvent.ACTION_UP:
                mCanvas.drawColor(fontColor);
                mCanvas.drawBitmap(tempBitmap, 0, 0, null);
                break;
        }
        invalidate();
        return true;
    }

    public int getTX(){
        int x;
        x = tx+200;
        x=tx*9/400;
        x+=5;
        return x;
    }

    public int getTY(){
        int y;
        y = ty+200;
        y=ty*9/400;
        y+=5;
        return y;
    }
    public int getTD(){
        return intD;
    }
}

